/* Program Name: myls
 * Author: Cooper965
 *
 * This program prints out the contents (files, directories, links, etc.)
 * of the optional directories provided to it. It has the following
 * signature:
 *
 *     myls [-flags] [dir]...,
 *
 * where "dir" represents a valid directory path on the system. The ellipsis
 * (...) afterwards indicates that multiple directories ("dir"'s) can be
 * provided. If that's the case, then the contents of each is printed out
 * in alphabetical order (by default) with the directory of interest shown.
 *
 * If no "dir" is provided, then the directory where "myls" is called from
 * is used.
 *
 * The following flags are described:
 *     -a, --all:   This flag displays "hidden" files, which are files
 *                  beginning with a period, e.g., .bashrc
 *
 *     -w, --width: This flag (followed by a required argument)
 *                  provides the number of characters allowed for the
 *                  results to be displayed. Minimum is one column
 *                  no matter what
 *
 * This program is intended to be a recreation of the coreutils ls command
 * from scratch. Eventually, the hope is that it will contain the full
 * functionality of the actual ls command.
 *
 * Notes:
 *     1. The strcoll isn't quite matching ls output with #'s
 *        Ex. "ls /etc" -> Look at the ordering of sensors.d w.r.t sensors3.conf
 *
 * Todo:
 *     1. Place argument processing into its own function from main()
 *     2. Need to expand error handling
 *     3. Constantly iterate on better ways to structure this code
 */

/* ==================== */
/*    Preprocessing     */
/* ==================== */
#include <ctype.h>
#include <dirent.h>
#include <error.h>
#include <errno.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

/* ==================== */
/*     Global Defs      */
/* ==================== */
#define CWD     "."       // Path provided to myls if no argument given
#define HIDDEN  '.'       // Beginning character that signifies hidden content
#define COL_PAD 2         // Default padding between listing columns

/* Defines data type for keeping track of possible flag values */
typedef struct Flags {
    bool all;
    int width;
} flags;

/* ==================== */
/*     Declarations     */
/* ==================== */
/* -------------------- */
/* Typedef Declarations */
/* -------------------- */
typedef int (*filter_fn_t)(const struct dirent *);                           // Simplify filter syntax
typedef int (*compar_fn_t)(const struct dirent **, const struct dirent **);  // Simplify compar syntax

/* --------------------- */
/* Function Declarations */
/* --------------------- */
/* Checks whether a string is composed of only digits */
bool is_input_digit(const char *);

/* Determines if dir entry is "hidden" or not, i.e., begins with '.' */
int is_not_hidden(const struct dirent *);

/* Lowercases a string: returns heap-allocated version */
char *lower_string(const char *);

/* Sorts caseless versions of dir entries by alphabetical order */
int caseless_alphasort(const struct dirent **, const struct dirent **);

/* Free the linked-list of dir entries */
void free_dir_array(struct dirent **, int);

/* Get the number of characters (in width) of the active terminal */
size_t get_terminal_width(void);

/* Get character widths for each printing column for dir entries */
int get_col_widths(struct dirent **, const int, const int, int *);

/* Get the directory contents with defined filter and sorting in the flags */
int extract_dir_contents(const char *, struct dirent ***, flags *);

/* Print the results of the directory contents in manner requested by flags */
void print_dir_entries(struct dirent **, int, flags *);

/* Gets the dir entries for given path and reports them to stdout */
void listdir(const char *, flags *);

/* ==================== */
/*    Main Function     */
/* ==================== */
int main(int argc, char *argv[]) {
    /* Initialize the flags to 'false' until provided */
    flags call_flags = { .all = false, .width = 0 };

    /* Define long options to command-line input */
    int option_index = 0;
    static struct option long_options[] =
    {
        {"all",   no_argument,       0, 'a'},
        {"width", required_argument, 0, 'w'},
        {0, 0, 0, 0}
    };

    /* Parse provided flags into the flags data structure */
    int opt = getopt_long(argc, argv, "aw:", long_options, &option_index);
    while (opt != -1) {
        switch (opt)
        {
        case 'a':
            call_flags.all = true;
            break;
        case 'w':
            if (!is_input_digit(optarg)) {
                error(1, 0, "invalid line width: '%s'", optarg);
            }
            call_flags.width = atoi(optarg);
            break;
        default:
            return 1;
        }
        opt = getopt_long(argc, argv, "aw:", long_options, &option_index);
    }

    /* Cater the myls command depending on # of provided directories */
    if (optind < argc - 1) { /* For multiple provided directories */
        for (int i = optind; i < argc; i++) {
            printf("%s:\n", argv[i]);
            listdir(argv[i], &call_flags);
            printf("\n");
        }
    } else { /* For either a single or no directories */
        listdir(optind == argc - 1 ? argv[optind] : CWD, &call_flags);
    }
    return EXIT_SUCCESS;
}

/* ==================== */
/*   Helper Functions   */
/* ==================== */
/* Function: is_input_digit
 * ------------------------------
 * This function returns true if the entire input
 * string is composed of digits, e.g.,:
 *     "12345534".
 *
 * It returns false if there is ANY character in
 * the input string that is not defined as a digit
 * by isdigit(), e.g.,:
 *     "-23".
 */
bool is_input_digit(const char *input) {
    bool is_digit = true;      // Initialize the return value to true.
    for (int i = 0; i < strlen(input); i++) {
        if (!isdigit(input[i])) {
            is_digit = false;  // If non-digit found, set return to false, and
            break;             // break out of the loop.
        }
    }
    return is_digit;
}

/* Function: is_not_hidden
 * ------------------------------
 * This function returns 1 (True) if the provided content does not
 * begin with a '.', which means it's a hidden file. If it does
 * begin with a '.', then it returns 0 (False). Is a filter for scandir().
 *
 * Assume that dirptr is a valid dirent struct pointer.
 */
int is_not_hidden(const struct dirent *dirptr) {
    return dirptr->d_name[0] != HIDDEN;
}

/* Function: lower_string
 * ------------------------------
 * This function takes a string as argument and returns an
 * identical string with all characters converted to lowercase.
 *
 * The returned string is heap-allocated and must be freed.
 */
char *lower_string(const char *string) {
    /* Remove leading '.' if present */
    if (string[0] == HIDDEN) {
        string++;
    }
    
    /* Heap allocate a copy of the provided string */
    char *lowered = strdup(string);

    /* For each character in the heap-allocated string, make lowercase */
    for (size_t i = 0; i < strlen(lowered); i++) {
        lowered[i] = tolower((unsigned char)lowered[i]);
    }
    return lowered;
}

/* Function: caseless_alphasort
 * ------------------------------
 * This function takes two dir entries, makes their names lowercase, and compares
 * them to one another in the standard method of strcmp(). Note that strcoll() is
 * used instead for increased portability.
 *
 * If dir0->d_name > dir1->d_name, then a positive int is returned. If the converse,
 * then a negative int is returned. Otherwise, they are identical and 0 is returned.
 */
int caseless_alphasort(const struct dirent **dir0, const struct dirent **dir1) {
    /* Lowercase the names of the dir entries */
    char *caseless_dir0 = lower_string((*(dir0))->d_name);
    char *caseless_dir1 = lower_string((*(dir1))->d_name);

    /* Compare the lowercased names */
    int cmp = strcoll(caseless_dir0, caseless_dir1);

    /* Free the heap-allocated, lowercase names */
    free(caseless_dir0);
    free(caseless_dir1);

    return cmp;
}

/* Function: free_dir_array
 * ------------------------------
 * This function frees a heap-allocated array of dirent structs.
 * Assumes that dir_array is a heap-allocated pointer.
 */
void free_dir_array(struct dirent **dir_array, int dir_entries) {
    /* Free each dirent struct pointer contained in the array */
    for (int i = 0; i < dir_entries; i++) {
        free(dir_array[i]);
    }
    /* Free the overarching array */
    free(dir_array);
}

/* Function: get_terminal_width
 * ------------------------------
 * This function gets the current size of the terminal window in
 * terms of width of characters that can be represented by it.
 */
size_t get_terminal_width(void) {
    /* Define winsize structure and extract terminal dims to it */
    struct winsize window;
    ioctl(0, TIOCGWINSZ, &window);

    return window.ws_col;
}

/* Function: get_col_widths
 * ------------------------------
 * This function gets the number of characters for each column of dir entries
 * that are to be filled in order to maximize the number of columns that the
 * entries are organized into across the provided "char_width".
 *
 * Function assumes that the array "col_widths" has a length of >= "num_dir".
 *
 * Notes: Look into splitting this function up and cleaning it up.
 */
int get_col_widths(struct dirent **dir_list, const int num_dir, const int char_width, int *col_widths) {
    /* Collect the lengh of each dir entry with padding */
    int print_lengths[num_dir];
    for (int i = 0; i < num_dir; i++) {
        print_lengths[i] = strlen(dir_list[i]->d_name) + COL_PAD;
    }

    /* Iterate over number of columns to print to find the minimum allowable */
    int columns = num_dir;                     // Need to define outside of loop to return
    for (; columns > 0; columns--) {
        /* Gets the number of rows for each column */
        int rows = num_dir % columns ? num_dir / columns + 1 : num_dir / columns;

        /* Makes sure there are no empty columns */
        if (rows * (columns - 1) <= num_dir) {
            int remaining_width = char_width;  // Tracks remaining terminal width available for columns
            int dir_idx = 0;                   // Index for current directory entry

            /* For each column, find the maximum word length */
            for (int col = 0; col < columns; col++) {
                int col_len = 0;               // Length (in characters) of the current column

                /* For each entry in the column, get the maximum length */
                for (int row = 0; row < rows; row++) {
                    if (dir_idx < num_dir) {
                        col_len = print_lengths[dir_idx] > col_len ? print_lengths[dir_idx] : col_len;
                        dir_idx++;
                    } 
                }
                col_widths[col] = col_len;
                remaining_width -= col_len;
            }
            /* Correct widths of last entry to get rid of padding */
            col_widths[columns - 1] -= COL_PAD;
            remaining_width -= COL_PAD;

            /* If not more characters than available used, return the column length */
            if (remaining_width >= 0) {
                break;
            }
        }
    }
    return columns;
}

/* Function: extract_dir_entries
 * ------------------------------
 * Takes a provided directory path (dirpath) and uses the filter and sorting
 * implied by the provided flags to extract the files, directories, and links
 * at that directory path.
 *
 * Note that "namelist" will be heap allocated and needs to be freed after
 * use external to this function.
 */
int extract_dir_entries(const char *dirpath, struct dirent ***namelist, flags *call_flags) {
    /* Initialize filter to not display hidden values and compare to sort alphabetically */
    filter_fn_t filter_fun = is_not_hidden;
    compar_fn_t compar_fun = caseless_alphasort;

    /* Check flags to modify the filter and ordering of directory contents */
    if (call_flags->all) {
        filter_fun = NULL;
    }

    /* Extract contents (directories and files) from dirpath */
    int dir_entries = scandir(dirpath, namelist, filter_fun, compar_fun);

    /* If an error occurred, then send error message and return */
    if (dir_entries < 0) {
	    error(errno, 0, "cannot access '%s': No such file or directory", dirpath);
    }
    return dir_entries;
}

/* Function: print_dir_entries
 * ------------------------------
 * Prints the contents of the provided directory to stdout in
 * the manner indicated by the appropriate flags.
 *
 * Assumes that "dir_entries" corresponds to the number of elements
 * in "namelist".
 */
void print_dir_entries(struct dirent **namelist, int dir_entries, flags *call_flags) {
    /* Get the allowable terminal width: defaults to the width of the whole terminal */
    int char_width = get_terminal_width();
    if (call_flags->width) {
        char_width = call_flags->width;
    }

    /* Output the dir entries for the provided "dirpath" */
    int col_widths[dir_entries];
    int num_cols = get_col_widths(namelist, dir_entries, char_width, col_widths);

    // Note: Make get_num_rows its own function and use in get_col_widths()
    int num_rows = dir_entries % num_cols ? dir_entries / num_cols + 1 : dir_entries / num_cols;
    int offset = dir_entries % num_rows;
    for (int row = 0; row < num_rows; row++) {
        int print_elements = ((row + 1) > offset && offset) ? num_cols - 1 : num_cols;

        for (int el = 0; el < print_elements; el++) {
            printf("%-*s", col_widths[el], namelist[row + num_rows * el]->d_name);
        }
        printf("\n");
    }
}

/* Function: listdir
 * ------------------------------
 * For a given directory ("dirpath"), get the contents of that path using
 * scandir() in order defined by "compar_fun" and filtered by "filter_fnc".
 *
 * Assumes that "dirpath" is a valid C-string.
 */
void listdir(const char *dirpath, flags *call_flags) {
    /* Get the contents of directory */
    struct dirent **namelist = NULL;
    int dir_entries = extract_dir_entries(dirpath, &namelist, call_flags);

    /* Print the results as requested */
    print_dir_entries(namelist, dir_entries, call_flags);

    /* Free the heap-allocated array of content */
    free_dir_array(namelist, dir_entries);
}
